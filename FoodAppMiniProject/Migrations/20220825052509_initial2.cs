﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FoodAppMiniProject.Migrations
{
    public partial class initial2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Dish_Admin_AdminId",
                table: "Dish");

            migrationBuilder.DropIndex(
                name: "IX_Dish_AdminId",
                table: "Dish");

            migrationBuilder.DropColumn(
                name: "AdminId",
                table: "Dish");

            migrationBuilder.AddColumn<DateTime>(
                name: "OrderDate",
                table: "Order",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "AddedOn",
                table: "Dish",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "Dish",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateIndex(
                name: "IX_Dish_AddedBy",
                table: "Dish",
                column: "AddedBy");

            migrationBuilder.AddForeignKey(
                name: "FK_Dish_Admin_AddedBy",
                table: "Dish",
                column: "AddedBy",
                principalTable: "Admin",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Dish_Admin_AddedBy",
                table: "Dish");

            migrationBuilder.DropIndex(
                name: "IX_Dish_AddedBy",
                table: "Dish");

            migrationBuilder.DropColumn(
                name: "OrderDate",
                table: "Order");

            migrationBuilder.DropColumn(
                name: "AddedOn",
                table: "Dish");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "Dish");

            migrationBuilder.AddColumn<int>(
                name: "AdminId",
                table: "Dish",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Dish_AdminId",
                table: "Dish",
                column: "AdminId");

            migrationBuilder.AddForeignKey(
                name: "FK_Dish_Admin_AdminId",
                table: "Dish",
                column: "AdminId",
                principalTable: "Admin",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

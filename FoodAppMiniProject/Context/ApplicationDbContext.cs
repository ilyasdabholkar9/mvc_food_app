﻿using FoodAppMiniProject.Models;
using Microsoft.EntityFrameworkCore;

namespace FoodAppMiniProject.Context
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> Context):base(Context)
        {
        }

        public DbSet<Admin> Admin { get; set; }
        public DbSet<User> User { get; set; }
        public DbSet<Dish> Dish { get; set; }
        public DbSet<Cart> Cart { get; set; }
        public DbSet<Order> Order { get; set; }
        public DbSet<FoodAppMiniProject.Models.ProcessOrderViewModel>? ProcessOrderViewModel { get; set; }
        


    }
}

﻿namespace FoodAppMiniProject.Helpers
{
    public class ImageHelper
    {

        public readonly Microsoft.AspNetCore.Hosting.IHostingEnvironment environment;
        public ImageHelper(Microsoft.AspNetCore.Hosting.IHostingEnvironment environment)
        {
            this.environment = environment;
        }

        public string UploadFile(IFormFile file)
        {
            var ext = Path.GetExtension(file.FileName);
            string name = Path.GetFileNameWithoutExtension(file.FileName);
            string myfile = name + "_" + Guid.NewGuid().ToString() + ext;
            var filepath = Path.Combine(environment.ContentRootPath, @"wwwroot\images", myfile);
            using var fileStream = new FileStream(filepath, FileMode.Create);
            file.CopyTo(fileStream);
            return filepath;
        }

    }
}

﻿namespace FoodAppMiniProject.Services
{
    public interface IFileUploadService
    {
            string UploadImage(IFormFile file);

    }
}

﻿using FoodAppMiniProject.Models;
using FoodAppMiniProject.Repository;

namespace FoodAppMiniProject.Services
{
    public class CartService : ICartService
    {
        readonly ICartInterface _cartRepository;

        public CartService(ICartInterface repo)
        {
            _cartRepository = repo;
        }

        public bool AddToCart(Cart c)
        {
                bool status = _cartRepository.AddToCart(c);
                return status;
        }

        public bool DeleteFromCart(int id)
        {
            return _cartRepository.DeleteFromCart(id);
        }

        public List<Cart> GetAllCartItems(int id)
        {
            return _cartRepository.GetAllCartItems(id);
        }

        public bool DeleteFromCartBulk(List<Cart> items)
        {
            return _cartRepository.DeleteFromCartBulk(items);
        }

        public bool DecrementCartItem(int id)
        {
            return _cartRepository.DecrementCartItem(id);
        }

        public bool IncrementCartItem(int id)
        {
            return _cartRepository.IncrementCartItem(id);
        }

        public Cart InCart(int id)
        {
            return _cartRepository.InCart(id);
        }
    }
}

﻿using FoodAppMiniProject.Models;

namespace FoodAppMiniProject.Services
{
    public interface ICartService
    {
        public List<Cart> GetAllCartItems(int id);
        public bool AddToCart(Cart c);
        public bool DeleteFromCart(int id);

        public bool DeleteFromCartBulk(List<Cart> items);
        public bool IncrementCartItem(int id);

        public bool DecrementCartItem(int id);

        public Cart InCart(int id);
    }
}

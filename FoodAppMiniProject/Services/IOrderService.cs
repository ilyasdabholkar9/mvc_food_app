﻿using FoodAppMiniProject.Models;

namespace FoodAppMiniProject.Services
{
    public interface IOrderService
    {
        public bool AddOrder(Order o);

        public List<Order> GetAllOrders(int userid);

        public bool AddOrderBulk(List<Order> o);

        public List<Order> GetDistinctInvoices(int userid);

        public List<Order> GetAllOrders();

        public bool UpdateOrder(Order o);

    }
}

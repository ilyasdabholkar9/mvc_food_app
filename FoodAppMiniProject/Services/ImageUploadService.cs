﻿namespace FoodAppMiniProject.Services
{
    public class ImageUploadService : IFileUploadService
    {
        public readonly Microsoft.AspNetCore.Hosting.IHostingEnvironment environment;
        public ImageUploadService(Microsoft.AspNetCore.Hosting.IHostingEnvironment environment)
        {
            this.environment = environment;
        }

        public string UploadImage(IFormFile file)
        {
            var ext = Path.GetExtension(file.FileName);
            string name = Path.GetFileNameWithoutExtension(file.FileName);
            string myfile = name + "_" + Guid.NewGuid().ToString() + ext;
            var filepath = @"~/images/" + myfile;
            var uploadPath = Path.Combine(environment.ContentRootPath, @"wwwroot\images", myfile);
            using var fileStream = new FileStream(uploadPath, FileMode.Create);
            file.CopyTo(fileStream);
            return filepath;
        }
    }

}

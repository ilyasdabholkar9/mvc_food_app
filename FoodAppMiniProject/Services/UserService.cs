﻿using FoodAppMiniProject.Models;
using FoodAppMiniProject.Repository;

namespace FoodAppMiniProject.Services
{
    public class UserService : IUserService
    {
        readonly IUserRepository _userRepository;

        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public bool CreateNewUser(User u)
        {
            _userRepository.CreateNewUser(u);
            return true;
        }

        public bool DeleteUser(int id)
        {
            return _userRepository.DeleteUser(id);
        }

        public List<User> GetAllUsers()
        {
            return _userRepository.GetAllUsers();
        }

        public User GetUserById(int id)
        {
            throw new NotImplementedException();
        }

        public bool UpdateUser(User u)
        {
            throw new NotImplementedException();
        }

        public User FindByEmail(string email)
        {
            return _userRepository.FindByEmail(email);
        }
    }
}

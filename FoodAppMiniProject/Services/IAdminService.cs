﻿using FoodAppMiniProject.Models;

namespace FoodAppMiniProject.Services
{
    public interface IAdminService
    {
        public List<Admin> GetAllUsers();
        public bool UpdateUser(Admin a);
        public Admin GetUserById(int id);
        public bool CreateNewUser(Admin a);
        public Admin FindByEmail(string email);

    }
}

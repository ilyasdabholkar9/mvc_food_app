﻿using FoodAppMiniProject.Models;
using FoodAppMiniProject.Repository;

namespace FoodAppMiniProject.Services
{
    public class AdminService : IAdminService
    {
        readonly IAdminRepository _adminRepository;

        public AdminService(IAdminRepository adminRepository)
        {
            _adminRepository = adminRepository;
        }

        public bool CreateNewUser(Admin a)
        {
            throw new NotImplementedException();
        }

        public Admin FindByEmail(string email)
        {
            return _adminRepository.FindByEmail(email);
        }

        public List<Admin> GetAllUsers()
        {
            throw new NotImplementedException();
        }

        public Admin GetUserById(int id)
        {
            throw new NotImplementedException();
        }

        public bool UpdateUser(Admin a)
        {
            throw new NotImplementedException();
        }
    }
}

﻿using FoodAppMiniProject.Models;

namespace FoodAppMiniProject.Services
{
    public interface IUserService
    {
        public List<User> GetAllUsers();
        public bool UpdateUser(User u);
        public User GetUserById(int id);
        public bool CreateNewUser(User u);

        public bool DeleteUser(int id);

        public User FindByEmail(string email);
    }
}

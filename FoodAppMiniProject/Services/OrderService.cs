﻿using FoodAppMiniProject.Models;
using FoodAppMiniProject.Repository;

namespace FoodAppMiniProject.Services
{
    public class OrderService : IOrderService
    {
        readonly IOrderRepository _orderRepository;

        public OrderService(IOrderRepository orderRepository)
        {
            _orderRepository = orderRepository;
        }

        public bool AddOrder(Order o)
        {
            return _orderRepository.AddOrder(o);
        }

        public List<Order> GetAllOrders(int userid)
        {
            return _orderRepository.GetAllOrders(userid);
        }

        public bool AddOrderBulk(List<Order> o)
        {
            return _orderRepository.AddOrderBulk(o);
        }

        public List<Order> GetDistinctInvoices(int userid)
        {
            return _orderRepository.GetDistinctInvoices(userid);
        }

        public List<Order> GetAllOrders()
        {
            return _orderRepository.GetAllOrders();
        }

        public bool UpdateOrder(Order o)
        {
            return _orderRepository.UpdateOrder(o);
        }
    }
}

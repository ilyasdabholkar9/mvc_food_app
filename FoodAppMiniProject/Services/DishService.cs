﻿using FoodAppMiniProject.Models;
using FoodAppMiniProject.Repository;

namespace FoodAppMiniProject.Services
{
    public class DishService : IDishService
    {
        readonly IDishRepository _dishRepository;

        public DishService(IDishRepository dishRepository)
        {
            _dishRepository = dishRepository;
        }

        public bool CreateNewDish(Dish d)
        {
            try
            {
                _dishRepository.CreateNewDish(d);
                return true;
            }
            catch(Exception e)
            {
                return false;
            }
        }

        public bool DeleteDish(int id)
        {
            return _dishRepository.DeleteDish(id);
        }

        public List<Dish> GetAllDishes()
        {
            return _dishRepository.GetAllDishes();
        }

        public Dish GetDishById(int id)
        {
            throw new NotImplementedException();
        }

        public bool UpdateDish(Dish d)
        {
            _dishRepository.UpdateDish(d);
            return true;
        }
    }
}

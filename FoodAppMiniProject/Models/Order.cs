﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FoodAppMiniProject.Models
{
    public class Order
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string Invoice { get; set; }

        public int UserId { get; set; }
       
        public int DishId { get; set; }

        public int Qantity { get; set; }

        public double Price { get; set; }

        public string Status { get; set; }

        public DateTime OrderDate { get; set; } = DateTime.Now;

        [ForeignKey("UserId")]
        public virtual User User { get; set; }
       
        [ForeignKey("DishId")]
        public virtual Dish Dish { get; set; }

    }
}

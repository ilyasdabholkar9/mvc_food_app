﻿using System.ComponentModel.DataAnnotations;

namespace FoodAppMiniProject.Models
{
    public class ProcessOrderViewModel
    {
        [Key]
        public int Id { get; set; }
        public string OrderStatus { get; set; }

    }
}

﻿using System.ComponentModel.DataAnnotations;

namespace FoodAppMiniProject.Models
{
    public class LoginViewModel
    {
        [Key]
        [Required]
        [RegularExpression(@"^([a-zA-Z0-9._%-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4})*$", ErrorMessage = "Please enter correct email")]
        public string Email { get; set; }

        [Required]
        [RegularExpression("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,12}$", ErrorMessage = "Please enter a vaild password")]
        public string Password { get; set; }
    }
}

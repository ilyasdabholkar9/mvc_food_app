﻿using System.ComponentModel.DataAnnotations;

namespace FoodAppMiniProject.Models
{
    public class AddDishViewModel
    {
        [Key]
        [Required]
        [StringLength(30, MinimumLength = 3)]
        [RegularExpression(@"^[a-zA-Z ]*$", ErrorMessage = "Please enter a valid dish name")]
        public string DishName { get; set; }

        [Required]
        [StringLength(100, MinimumLength = 3)]
        [RegularExpression(@"^[a-zA-Z0-9 ]*$", ErrorMessage = "Please enter a description")]
        public string About { get; set; }

        [Required]
        [RegularExpression(@"\d+.?\d+", ErrorMessage = "Please enter a valid price")]
        public double Price { get; set; }

    }
}

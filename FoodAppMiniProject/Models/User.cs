﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FoodAppMiniProject.Models
{
    public class User
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [StringLength(30, MinimumLength = 3)]
        [RegularExpression(@"^[a-zA-Z ]*$",ErrorMessage = "Please enter a valid name")]
        public string Name { get; set; }

        [Required]
        [Remote("doesUserEmailExist", "User",HttpMethod ="POST",ErrorMessage="User with this email already exists")]
        [RegularExpression(@"^([a-zA-Z0-9._%-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4})*$",ErrorMessage = "Please enter correct email")]
        public string Email { get; set; }
        
        [Required]
        [RegularExpression(@"^([0]|\+91)?[789]\d{9}$", ErrorMessage = "Please enter a valid phone no")]
        public string Phone { get; set; }

        [DataType("Password")]
        [Required]
        [RegularExpression("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,60}$", ErrorMessage = "Password Must contain atleast one Uppercase letter,lowercase letter, digit and symbol")]
        public string Password { get; set; }
        
        [Required]
        [StringLength(100, MinimumLength = 5)]
        public string Address { get; set; }

    }
}

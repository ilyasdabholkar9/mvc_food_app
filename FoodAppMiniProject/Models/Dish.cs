﻿using FoodAppMiniProject.Context;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FoodAppMiniProject.Models
{
    public class Dish
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [StringLength(30, MinimumLength = 3)]
        [RegularExpression(@"^[a-zA-Z ]*$", ErrorMessage = "Please enter a valid dish name")]
        public string DishName { get; set; }

        [Required]
        [StringLength(100, MinimumLength = 3)]
        [RegularExpression(@"^[a-zA-Z0-9 ]*$", ErrorMessage = "Please enter a description")]
        public string About { get; set; }

        [Required]
        [RegularExpression(@"\d+.?\d+", ErrorMessage = "Please enter a valid price")]
        public double Price { get; set; }

        public int AddedBy { get; set; }

        public string Img { get; set; }

        public bool IsDeleted { get; set; } = false;

        public DateTime AddedOn { get; set; } = DateTime.Now;

        [ForeignKey("AddedBy")]
        public virtual Admin Admin { get; set; }
    }
}

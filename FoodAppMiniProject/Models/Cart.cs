﻿using FoodAppMiniProject.Context;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FoodAppMiniProject.Models
{
    public class Cart
    {


        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public int UserId { get; set; }
        
        public int DishId { get; set; }

        public int Quantity { get; set; }

        [ForeignKey("UserId")]
        public virtual User User { get; set; }

        [ForeignKey("DishId")]
        public virtual Dish Dish { get; set; }

    }
}

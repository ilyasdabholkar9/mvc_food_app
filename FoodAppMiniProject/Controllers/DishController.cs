﻿using FoodAppMiniProject.Helpers;
using FoodAppMiniProject.Models;
using FoodAppMiniProject.Services;
using Microsoft.AspNetCore.Mvc;

namespace FoodAppMiniProject.Controllers
{
    public class DishController : Controller
    {
        private readonly IDishService _dishService;
        private readonly IFileUploadService _ImageUploadService;

        public DishController(IDishService dishService, IFileUploadService ImageUploadService)
        {
            _dishService = dishService;
            _ImageUploadService = ImageUploadService;
        }

        [HttpGet]
        public IActionResult ViewDishes()
        {
            var admin = SessionHelper.GetObjectFromJson<Admin>(HttpContext.Session, "admin");
            if (admin != null)
            {
                ViewBag.Admin = admin.Name;
            }
            else
            {
                return RedirectToRoute(new { controller = "Admin", action = "AdminLogin" });
            }
            return View(_dishService.GetAllDishes());
        }

        [HttpGet]
        public IActionResult AddDish()
        {
            var user = SessionHelper.GetObjectFromJson<User>(HttpContext.Session, "admin");
            if (user != null)
            {
                ViewBag.Admin = user.Name;
            }
            else
            {
                return RedirectToRoute(new { controller = "Admin", action = "AdminLogin" });
            }
            return View();
        }

        [HttpPost]
        public IActionResult AddDish(IFormFile file,AddDishViewModel viewModel)
        {
            string Filepath = "";
            if (file != null)
            {
                Filepath = _ImageUploadService.UploadImage(file);
            }
            var admin = SessionHelper.GetObjectFromJson<Admin>(HttpContext.Session, "admin");

            Dish dish = new Dish() { DishName = viewModel.DishName, About = viewModel.About, Price = viewModel.Price };
            dish.AddedBy= admin.Id;
            dish.Img = Filepath;
            _dishService.CreateNewDish(dish);
            TempData["type"] = "success";
            TempData["msg"] = "Dish Added Successfully";
            return RedirectToAction("AddDish");
        }


        [HttpGet]
        public IActionResult DeleteDish(int id)
        {
            _dishService.DeleteDish(id);
            return RedirectToAction("ViewDishes");
        }

        [HttpGet]
        public IActionResult UpdateDish(int id)
        {
            var user = SessionHelper.GetObjectFromJson<User>(HttpContext.Session, "admin");
            if (user != null)
            {
                ViewBag.Admin = user.Name;
            }
            else
            {
                return RedirectToRoute(new { controller = "Admin", action = "AdminLogin" });
            }
            var dishes = _dishService.GetAllDishes().ToList();
            Dish dish = dishes.Where(item=>item.Id == id).FirstOrDefault();
            return View(dish);
        }

        [HttpPost]
        public IActionResult UpdateDish(IFormFile file, Dish viewModel)
        {
            string Filepath = "";
            if (file != null)
            {
                Filepath = _ImageUploadService.UploadImage(file);
            }
            var admin = SessionHelper.GetObjectFromJson<Admin>(HttpContext.Session, "admin");

            List<Dish> dishes = _dishService.GetAllDishes();
            Dish dish = dishes.Where(item => item.Id == viewModel.Id).FirstOrDefault();
            dish.DishName = viewModel.DishName;
            dish.About = viewModel.About;
            dish.Price = viewModel.Price;
            if (file!=null)
            {
                dish.Img = Filepath;
            }
            _dishService.UpdateDish(dish);
            TempData["type"] = "success";
            TempData["msg"] = "Dish Updated Successfully";
            return RedirectToAction("ViewDishes");
        }

        public IActionResult Details(int id)
        {
            var admin = SessionHelper.GetObjectFromJson<Admin>(HttpContext.Session, "admin");
            if (admin != null)
            {
                ViewBag.Admin = admin.Name;
            }
            else
            {
                return RedirectToRoute(new { controller = "Admin", action = "AdminLogin" });
            }

            Dish dish = _dishService.GetAllDishes().Where(i => i.Id == id).FirstOrDefault();
            return View(dish);
        }
    }
}

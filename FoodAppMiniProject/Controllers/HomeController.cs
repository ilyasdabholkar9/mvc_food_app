﻿using FoodAppMiniProject.Models;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using FoodAppMiniProject.Helpers;
using FoodAppMiniProject.Services;

namespace FoodAppMiniProject.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IDishService _dishService;

        public HomeController(ILogger<HomeController> logger,IDishService ds)
        {
            _logger = logger;
            _dishService = ds;
        }

        
        public IActionResult Index()
        {
            var user = SessionHelper.GetObjectFromJson<User>(HttpContext.Session, "user");
            List<Dish> dishes;
            if (user != null)
            {
               dishes = _dishService.GetAllDishes();
               ViewBag.User = user.Name;
                
            }
            else 
            {
                return RedirectToRoute(new { controller = "User", action = "UserLogin" });
            }
            return View(dishes);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
﻿using FoodAppMiniProject.Helpers;
using FoodAppMiniProject.Models;
using FoodAppMiniProject.Services;
using Microsoft.AspNetCore.Mvc;

namespace FoodAppMiniProject.Controllers
{
    public class OrderController : Controller
    {
        IOrderService _orderService;
        ICartService _cartService;

        public OrderController(IOrderService userService,ICartService cartservice)
        {
            _orderService = userService;
            _cartService = cartservice;
        }

        [HttpGet]
        public IActionResult Checkout()
        {
            var user = SessionHelper.GetObjectFromJson<User>(HttpContext.Session, "user");
            List<Cart> items = _cartService.GetAllCartItems(user.Id);
            string invoiceNo = Guid.NewGuid().ToString();
            List<Order> orders = new List<Order>();
            items.ForEach(item =>
            {
                orders.Add(new Order()
                {
                    DishId = item.DishId,
                    Invoice = invoiceNo,
                    Qantity = item.Quantity,
                    UserId = user.Id,
                    Price = item.Dish.Price * item.Quantity,
                    Status = "Ordered"
                });
            });
            bool res = _orderService.AddOrderBulk(orders);
            if(res == true)
            {
                _cartService.DeleteFromCartBulk(items);
            }

            if(res == true)
            {
                TempData["type"] = "success";
                TempData["msg"] = "Order Placed Successfully";
            }
            return RedirectToRoute(new { controller = "Home", action = "Index" });
        }

        [HttpGet]
        public IActionResult ViewOrder()
        {
            var user = SessionHelper.GetObjectFromJson<User>(HttpContext.Session, "user");
            if (user != null)
            {
                ViewBag.User = user.Name;
            }
            else
            {
                return RedirectToRoute(new { controller = "User", action = "UserLogin" });
            }
            var olist = _orderService.GetAllOrders(user.Id).DistinctBy(item=>item.Invoice);
            return View(olist);
        }

        [HttpGet]
        public IActionResult ViewOrderDetails(string id)
        {
            Console.WriteLine("INVOICE : ##### ",id);
            var user = SessionHelper.GetObjectFromJson<User>(HttpContext.Session, "user");
            if (user != null)
            {
                ViewBag.User = user.Name;
            }
            else
            {
                return RedirectToRoute(new { controller = "User", action = "UserLogin" });
            }
            var olist = _orderService.GetAllOrders(user.Id).Where(item => item.Invoice == id).ToList();
            User u = olist.First().User;
            ViewBag.Address = u.Address;
            ViewBag.Invoice = id;
            double orderTotal = 0;
            olist.ForEach(o =>
            {
                orderTotal += o.Qantity * o.Dish.Price;
            });
            ViewBag.OrderTotal = orderTotal;
            return View(olist);
        }



        [HttpGet]
        public IActionResult ViewOrderAdmin()
        {
            var user = SessionHelper.GetObjectFromJson<User>(HttpContext.Session, "admin");
            if (user!= null)
            {
                ViewBag.Admin = user.Name;
            }
            else
            {
                return RedirectToRoute(new { controller = "Admin", action = "AdminLogin" });
            }
            var olist = _orderService.GetAllOrders();
            return View(olist);
        }


        [HttpGet]
        public IActionResult AdminOrderDetails(Order o)
        {
            var order = _orderService.GetAllOrders().ToList();
            return View(order.Where(item => item.Id == o.Id).FirstOrDefault());
        }

        [HttpGet]
        public IActionResult ProcessOrder(Order o)
        {
            //ProcessOrderViewModel orderModel = new ProcessOrderViewModel();
            //orderModel.Id = o.Id;
            //orderModel.OrderStatus = o.Status;
            return View(o);
        }

        [HttpPost]
        public IActionResult ProcessOrder(Order o,int id)
        {
            ProcessOrderViewModel orderModel = new ProcessOrderViewModel();
            var orders = _orderService.GetAllOrders();
            var order = orders.FirstOrDefault(item => item.Id == o.Id);
            
            order.Status = o.Status;
            _orderService.UpdateOrder(order);
            return RedirectToAction("ViewOrderAdmin");
        }

    }
}

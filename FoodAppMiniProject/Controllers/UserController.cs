﻿using FoodAppMiniProject.Models;
using FoodAppMiniProject.Services;
using Microsoft.AspNetCore.Mvc;
using FoodAppMiniProject.Helpers;
using Newtonsoft.Json;

namespace FoodAppMiniProject.Controllers
{
    public class UserController : Controller
    {
        IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var user = SessionHelper.GetObjectFromJson<User>(HttpContext.Session, "admin");
            if (user != null)
            {
                ViewBag.Admin = user.Name;
            }
            else
            {
                return RedirectToRoute(new { controller = "Admin", action = "AdminLogin" });
            }

            return View(_userService.GetAllUsers());
        }

        [HttpGet]
        public IActionResult NewUser()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Details(int id)
        {
            User u = _userService.GetAllUsers().Where(item=>item.Id==id).FirstOrDefault();
            return View(u);
        }


        [HttpPost]
        public IActionResult NewUser(User u)
        {
            if (ModelState.IsValid)
            {
                bool res = _userService.CreateNewUser(u);
                if (res == true)
                {
                    TempData["type"] = "success";
                    TempData["msg"] = "User Registered Successfully";
                }
            }
            return RedirectToAction("NewUser");
        }

        //LOGIN
        [HttpGet]
        public IActionResult UserLogin()
        {
            return View();
        }

        [HttpPost]
        public IActionResult UserLogin(LoginViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                Console.WriteLine("Invalid creds");
                return View();
            }

            var user = _userService.FindByEmail(viewModel.Email);
        
            if(user != null)
            {
                bool verified = BCrypt.Net.BCrypt.Verify(viewModel.Password, user.Password);
                if (verified)
                {
                    var userSession = SessionHelper.GetObjectFromJson<User>(HttpContext.Session, "user");
                    if (userSession == null)
                    {
                        SessionHelper.SetObjectAsJson(HttpContext.Session, "user", user);
                    }
                    return RedirectToRoute(new { controller = "Home", action = "Index" });
                }
                else
                {
                    TempData["type"] = "danger";
                    TempData["msg"] = "Invalid Email Or Password";
                }
                return View();
            }
            else
            {
                TempData["type"] = "danger";
                TempData["msg"] = "Invalid Email Or Password";
                return View();
            }
            
        }

        [HttpGet]
        public IActionResult UserLogout()
        {
            HttpContext.Session.Remove("user");
            return RedirectToAction("UserLogin");
        }


        //Remote validation
        [HttpPost]
        public JsonResult doesUserEmailExist(string email)
        {
            var user = _userService.FindByEmail(email);
            return Json(user == null);
        }

        [HttpGet]
        public IActionResult DeleteUser(int id)
        {

            var user = SessionHelper.GetObjectFromJson<User>(HttpContext.Session, "admin");
            if (user != null)
            {
                ViewBag.Admin = user.Name;
            }
            else
            {
                return RedirectToRoute(new { controller = "Admin", action = "AdminLogin" });
            }


            bool res = _userService.DeleteUser(id);
            if (res == true)
            {
                TempData["type"] = "danger";
                TempData["msg"] = "User Deleted";
            }
            return RedirectToAction("Index");
        }

    }
}

﻿using FoodAppMiniProject.Helpers;
using FoodAppMiniProject.Models;
using FoodAppMiniProject.Services;
using Microsoft.AspNetCore.Mvc;

namespace FoodAppMiniProject.Controllers
{
    public class CartController : Controller
    {
        private readonly ICartService _cartService;
        
        public CartController(ICartService cartService)
        {
            _cartService = cartService;
        }

        [HttpGet]
        public IActionResult ViewCart()
        {
            var user = SessionHelper.GetObjectFromJson<User>(HttpContext.Session, "user");
            if (user != null)
            {
                ViewBag.User = user.Name;
            }
            else
            {
                return RedirectToRoute(new { controller = "User", action = "UserLogin" });
            }
            List<Cart> c = _cartService.GetAllCartItems(user.Id);
            double cartTotal = 0;
            c.ForEach(item =>
            {
                cartTotal += item.Quantity * item.Dish.Price;
            });
            if(cartTotal > 0)
            {
                ViewBag.CartTotal = cartTotal;
            }
            return View(c);
        }

        [HttpGet]
        public IActionResult AddToCart(int id)
        {
            var user = SessionHelper.GetObjectFromJson<User>(HttpContext.Session, "user");
            Cart itemExists = _cartService.InCart(id);
            if (itemExists != null)
            {
                _cartService.IncrementCartItem(itemExists.Id);
            }
            else
            {
                Cart cart = new Cart()
                {
                    UserId = user.Id,
                    DishId = id,
                    Quantity = 1
                };
                bool res = _cartService.AddToCart(cart);
                if(res == true)
                {
                    TempData["type"] = "success";
                    TempData["msg"] = "Product Added To Cart";
                }
            }
            return RedirectToRoute(new { controller = "Home", action = "Index" });
        }

        [HttpGet]
        public IActionResult DeleteFromCart(int id)
        {
            _cartService.DeleteFromCart(id);
            return RedirectToRoute(new { controller = "Cart", action = "ViewCart" });
        }

        [HttpGet]
        public IActionResult IncrementCart(int id)
        { 
            _cartService.IncrementCartItem(id);
            return RedirectToAction("ViewCart");
        }

        [HttpGet]
        public IActionResult DecrementCart(int id)
        {
            _cartService.DecrementCartItem(id);
            return RedirectToAction("ViewCart");
        }
    }
}

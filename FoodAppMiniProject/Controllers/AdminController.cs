﻿using FoodAppMiniProject.Helpers;
using FoodAppMiniProject.Models;
using FoodAppMiniProject.Services;
using Microsoft.AspNetCore.Mvc;

namespace FoodAppMiniProject.Controllers
{
    public class AdminController : Controller
    {

        IAdminService _adminService;

        public AdminController(IAdminService userService)
        {
            _adminService = userService;
        }


        [HttpGet]
        public IActionResult AdminLogin()
        {
            return View();
        }

        [HttpPost]
        public IActionResult AdminLogin(AdminLoginViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                Console.WriteLine("Invalid creds");
                return View();
            }

            var admin = _adminService.FindByEmail(viewModel.Email);

            if (admin != null)
            {
                bool verified = BCrypt.Net.BCrypt.Verify(viewModel.Password, admin.Password);
                if (verified)
                {
                    var userSession = SessionHelper.GetObjectFromJson<User>(HttpContext.Session, "admin");
                    if (userSession == null)
                    {
                        SessionHelper.SetObjectAsJson(HttpContext.Session, "admin", admin);
                    }
                    return RedirectToRoute(new { controller = "Dish", action = "ViewDishes" });
                }
                else
                {
                    TempData["type"] = "danger";
                    TempData["msg"] = "Invalid Email Or Password";
                }
                return View();
            }
            else
            {
                TempData["type"] = "danger";
                TempData["msg"] = "Invalid Email Or Password";
                return View();
            }
        }

        [HttpGet]
        public IActionResult AdminLogout()
        {
            HttpContext.Session.Remove("admin");
            return RedirectToAction("AdminLogin");
        }


    }
}

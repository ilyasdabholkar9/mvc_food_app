﻿using FoodAppMiniProject.Context;
using FoodAppMiniProject.Models;

namespace FoodAppMiniProject.Repository
{
    public class AdminRepository : IAdminRepository
    {
        ApplicationDbContext _adminDbContext;

        public AdminRepository(ApplicationDbContext ctx)
        {
            _adminDbContext = ctx;
        }

        public bool CreateNewUser(Admin a)
        {
            throw new NotImplementedException();
        }

        public List<Admin> GetAllUsers()
        {
            throw new NotImplementedException();
        }

        public Admin GetUserById(int id)
        {
            throw new NotImplementedException();
        }

        public bool UpdateUser(Admin a)
        {
            throw new NotImplementedException();
        }

        public Admin FindByEmail(string email)
        {
            List<Admin> admins = _adminDbContext.Admin.ToList();
            return admins.Find(u => u.Email == email);
        }
    }
}

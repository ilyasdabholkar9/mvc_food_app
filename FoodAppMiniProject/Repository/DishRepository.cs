﻿using FoodAppMiniProject.Context;
using FoodAppMiniProject.Models;
using Microsoft.EntityFrameworkCore;

namespace FoodAppMiniProject.Repository
{
    public class DishRepository : IDishRepository
    {
        private readonly ApplicationDbContext _dishDbContext;
        public DishRepository(ApplicationDbContext ctx)
        {
            this._dishDbContext = ctx;
        }
        public bool CreateNewDish(Dish d)
        {
            try
            {
                _dishDbContext.Dish.Add(d);
                _dishDbContext.SaveChanges();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
            return true;
        }

        public bool DeleteDish(int id)
        {
            var dish = _dishDbContext.Dish.FirstOrDefault(item=> item.Id == id);
            dish.IsDeleted = true;
            _dishDbContext.SaveChanges();
            return true;

        }

        public List<Dish> GetAllDishes()
        {
            return _dishDbContext.Dish.Where(x => x.IsDeleted == false).Include(x=>x.Admin).ToList();
        }

        public Dish GetDishById(int id)
        {
            throw new NotImplementedException();
        }

        public bool UpdateDish(Dish d)
        {
            _dishDbContext.Entry<Dish>(d).State = EntityState.Modified;
            _dishDbContext.SaveChanges();
            return true;
        }
    }
}

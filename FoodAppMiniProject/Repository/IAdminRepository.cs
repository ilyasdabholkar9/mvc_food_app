﻿using FoodAppMiniProject.Models;

namespace FoodAppMiniProject.Repository
{
    public interface IAdminRepository
    {
        public List<Admin> GetAllUsers();
        public bool UpdateUser(Admin a);
        public Admin GetUserById(int id);
        public bool CreateNewUser(Admin a);
        public Admin FindByEmail(string email);
    }
}

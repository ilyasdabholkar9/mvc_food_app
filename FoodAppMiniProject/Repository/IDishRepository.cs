﻿using FoodAppMiniProject.Models;

namespace FoodAppMiniProject.Repository
{
    public interface IDishRepository
    {
        public List<Dish> GetAllDishes();
        public bool CreateNewDish(Dish d);
        public bool UpdateDish(Dish d);
        public bool DeleteDish(int id);
        public Dish GetDishById(int id);
    }
}

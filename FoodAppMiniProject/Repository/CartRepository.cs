﻿using FoodAppMiniProject.Context;
using FoodAppMiniProject.Models;
using Microsoft.EntityFrameworkCore;

namespace FoodAppMiniProject.Repository
{
    public class CartRepository : ICartInterface
    {
        private readonly ApplicationDbContext _cartDbContext;
        public CartRepository(ApplicationDbContext ctx)
        {
            this._cartDbContext = ctx;
        }
        public bool AddToCart(Cart c)
        {
            try
            {
                _cartDbContext.Cart.Add(c);
                _cartDbContext.SaveChanges();
                return true;
            }catch(Exception e)
            {
                return false;
            }
        }

        public Cart InCart(int id)
        {
            var cartItem =  _cartDbContext.Cart.FirstOrDefault(item => item.DishId == id);
            return cartItem != null ?  cartItem :  null;
           
        }

        public bool IncrementCartItem(int id)
        {
            try
            {
                var cartItem = _cartDbContext.Cart.FirstOrDefault(item => item.Id == id);
                int qty = cartItem.Quantity;
                cartItem.Quantity =  qty+1;
                _cartDbContext.SaveChanges();
                return true;
            }
            catch(Exception e)
            {
                return false;
            }
        }

        public bool DecrementCartItem(int id)
        {
            try
            {
                var cartItem = _cartDbContext.Cart.FirstOrDefault(item => item.Id == id);
                int qty = cartItem.Quantity;
                if(qty == 1)
                {
                    DeleteFromCart(id);
                    return true;
                }
                else
                {
                    cartItem.Quantity = qty - 1;
                    _cartDbContext.SaveChanges();
                    return true;
                }   
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public bool DeleteFromCart(int id)
        {
            List<Cart> items = _cartDbContext.Cart.ToList();
            Cart item = items.FirstOrDefault(item=>item.Id == id);
            _cartDbContext.Remove(item);
            _cartDbContext.SaveChanges();
            return true;
        }

        public List<Cart> GetAllCartItems(int id)
        { 
            var cartItems = _cartDbContext.Cart.Where(item => item.User.Id == id).Include(x=>x.Dish).ToList();
            return cartItems;
        }

        public bool DeleteFromCartBulk(List<Cart> items)
        {
            _cartDbContext.Cart.RemoveRange(items);
            _cartDbContext.SaveChanges();
            return true;
        }
    }
}

﻿using FoodAppMiniProject.Context;
using FoodAppMiniProject.Models;

namespace FoodAppMiniProject.Repository
{
    public class UserRepository : IUserRepository
    {
        ApplicationDbContext _userDbContext;

        public UserRepository(ApplicationDbContext ctx)
        {
            _userDbContext = ctx;
        }

        public bool CreateNewUser(User u)
        {
            try
            {
                string passwordHash = BCrypt.Net.BCrypt.HashPassword(u.Password);
                //Console.WriteLine($"Password Hash : {passwordHash}");
                u.Password = passwordHash;
                _userDbContext.User.Add(u);
                _userDbContext.SaveChanges();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
            return true;
        }

        public bool DeleteUser(int id)
        {
            try
            {
                User u = _userDbContext.User.Where(item => item.Id == id).FirstOrDefault();
                _userDbContext.Remove(u);
                _userDbContext.SaveChanges();
                return true;

            }
            catch(Exception e)
            {
                return false;
            }
        }

        public List<User> GetAllUsers()
        {
            return _userDbContext.User.ToList();
        }

        public User GetUserById(int id)
        {
            throw new NotImplementedException();
        }

        public bool UpdateUser(User u)
        {
            throw new NotImplementedException();
        }

        public User FindByEmail(string email)
        {
            List<User> users = _userDbContext.User.ToList();
            return users.Find(u => u.Email == email);
        }
    }
}

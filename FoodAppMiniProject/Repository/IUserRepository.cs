﻿using FoodAppMiniProject.Models;

namespace FoodAppMiniProject.Repository
{
    public interface IUserRepository
    {
        public bool CreateNewUser(User u);
        public bool UpdateUser(User u);
        public bool DeleteUser(int id);
        public User GetUserById(int id);
        public List<User> GetAllUsers();

        public User FindByEmail(string email);
    }
}

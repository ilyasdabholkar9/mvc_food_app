﻿using FoodAppMiniProject.Context;
using FoodAppMiniProject.Models;
using Microsoft.EntityFrameworkCore;

namespace FoodAppMiniProject.Repository
{
    public class OrderRepository : IOrderRepository
    {
        ApplicationDbContext _orderDbContext;

        public OrderRepository(ApplicationDbContext ctx)
        {
            _orderDbContext = ctx;
        }
        public bool AddOrder(Order o)
        {
            try
            {
                _orderDbContext.Order.Add(o);
                _orderDbContext.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public List<Order> GetAllOrders(int userid)
        {
            var orderItems = _orderDbContext.Order.Where(item => item.User.Id == userid).Include(x => x.Dish).Include(x=>x.User).ToList();
            return orderItems;
        }

        public bool AddOrderBulk(List<Order> o)
        {
            try
            {
                _orderDbContext.Order.AddRange(o);
                _orderDbContext.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }

        public List<Order> GetDistinctInvoices(int userid)
        {
                List<Order> ol = _orderDbContext.Order.Where(item=>item.User.Id == userid).Distinct().ToList();
                return ol;
        }

        public List<Order> GetAllOrders()
        {
            List<Order> ol = _orderDbContext.Order.Include(x=>x.User).Include(x=>x.Dish).ToList();
            return ol;
        }

        public bool UpdateOrder(Order o)
        {
            _orderDbContext.Entry<Order>(o).State = EntityState.Modified;
            _orderDbContext.SaveChanges();
            return true;
        }

    }
}

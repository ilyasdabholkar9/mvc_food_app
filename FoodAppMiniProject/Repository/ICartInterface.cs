﻿using FoodAppMiniProject.Models;

namespace FoodAppMiniProject.Repository
{
    public interface ICartInterface
    {
        public List<Cart> GetAllCartItems(int id);
        public bool AddToCart(Cart c);
        public bool DeleteFromCart(int id);

        public bool DeleteFromCartBulk(List<Cart> carts);

        public bool IncrementCartItem(int id);

        public bool DecrementCartItem(int id);
        public Cart InCart(int id);
    }
}

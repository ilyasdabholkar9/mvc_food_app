using FoodAppMiniProject.Context;
using FoodAppMiniProject.Repository;
using FoodAppMiniProject.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.FileProviders;

var builder = WebApplication.CreateBuilder(args);

var localConnectionString = builder.Configuration.GetConnectionString("LocalDbConnection");

// Add services to the container.
builder.Services.AddControllersWithViews();
builder.Services.AddSession();
builder.Services.AddScoped<IUserRepository, UserRepository>();
builder.Services.AddScoped<IUserService, UserService>();
builder.Services.AddScoped<IDishRepository, DishRepository>();
builder.Services.AddScoped<IDishService, DishService>();
builder.Services.AddScoped<IAdminRepository, AdminRepository>();
builder.Services.AddScoped<IAdminService, AdminService>();
builder.Services.AddScoped<IFileUploadService, ImageUploadService>();
builder.Services.AddScoped<ICartService, CartService>();
builder.Services.AddScoped<ICartInterface, CartRepository>();
builder.Services.AddScoped<IOrderService, OrderService>();
builder.Services.AddScoped<IOrderRepository, OrderRepository>();
builder.Services.AddDbContext<ApplicationDbContext>(u => u.UseSqlServer(localConnectionString));

var app = builder.Build();


// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();
app.UseSession();
app.UseRouting();

app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");
app.MapControllerRoute(
    name: "RegisterUser",
    pattern: "{controller=User}/{action=NewUser}/{id?}");
app.MapControllerRoute(
    name: "UserLogin",
    pattern: "{controller=User}/{action=UserLogin}");
app.MapControllerRoute(
    name: "AdminLogin",
    pattern: "{controller=Admin}/{action=AdminLogin}");
app.MapControllerRoute(
    name: "ViewDish",
    pattern: "{controller=Dish}/{action=ViewDishes}");
app.MapControllerRoute(
    name: "ViewCart",
    pattern: "{controller=Cart}/{action=ViewCart}");
app.MapControllerRoute(
    name: "ViewOrders",
    pattern: "{controller=Order}/{action=ViewOrder}");
app.MapControllerRoute(
    name: "ViewOrdersAdmin",
    pattern: "{controller=Order}/{action=ViewOrderAdmin}");


app.Run();
